# test

> A Vue.js project

## Build Setup

```bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report

# run unit tests
npm run unit

# run e2e tests
npm run e2e

# run all tests
npm test
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).

**启动后访问路径为**
[http://localhost:8080/pages/home/index.html#/](http://localhost:8080/pages/home/index.html#/)
[http://localhost:8080/pages/user/index.html#/](http://localhost:8080/pages/user/index.html#/)

**本脚手架基于 vue-cli 修改，支持多页面配置，如果要添加新页面请参照 src/pages/home 来添加**
<br>
**由于时间限制使用的 webpack3， 后期有时间会升级为 wepack4，如果哪位兄弟熟悉 webpack 的请帮忙升级下**
<br>
**代码检查用 eslint + prettier 完成，我自己设置的规范不一定都适合大家，后期最好商量使用一个统一的规范**
<br>
**本脚手架还有很多不足的地方，不足的地方请欢迎指正**

#地图功能实现
启动服务后地图页面访问路径：
[http://localhost:8080/pages/map/index.html#/](http://localhost:8080/pages/map/index.html#/)

##参考
[https://mapv.baidu.com/examples/baidu-map-point-heatmap-time.html](https://mapv.baidu.com/examples/baidu-map-point-heatmap-time.html)
**文档**
[https://github.com/huiyan-fe/mapv/blob/master/API.md](https://github.com/huiyan-fe/mapv/blob/master/API.md)
